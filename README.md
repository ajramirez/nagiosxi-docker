This is a repository for a docker container image of Nagios XI. It currently uses Nagios XI version 5.6.14. 
It is based on the code of this image:
[mavenquist/nagios-xi](https://hub.docker.com/r/mavenquist/nagios-xi/)

# Deployment
To get NagiosXI properly deployed, the following steps must be done:
* docker build . -t nagiosxi-g7
* docker run --privileged -p 80:80 -d --name nagiosxi-g7run nagiosxi-g7 /usr/sbin/init
* docker exec -ti nagiosxi-g7run bash ./finish.sh
* docker exec -ti nagiosxi-g7run bash run.sh

# IMPORTANT NOTE
This version lacks any persistence mechanisms. Therefore, every time the container is stopped, all the configuration which was done in Nagios is lost.
