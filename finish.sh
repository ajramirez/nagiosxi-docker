#!/bin/bash
./3-dbservers
./4-services
./5-sudoers
./6-firewall
./8-selinux
./9-dbbackups
./11-sourceguardian
./13-phpini
service mariadb start
./fix-ndoutils.sh
./A-subcomponents
./A0-mrtg
./B-installxi
./C-cronjobs
./D-chkconfigalldaemons
./E-importnagiosql
./F-startdaemons
./Z-webroot
